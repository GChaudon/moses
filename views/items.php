<!doctype html>
<html>
<head>
	<title>Liste des objets</title>
</head>
<body>
	<h1>Liste des objets</h1>
	<ul>
		<?php foreach($items as $item) { ?>
		<li><?= $item->getLabel() .' - '. $item->getPrice() .' €' ?></li>
		<?php } ?>
	</ul>
</body>
</html>