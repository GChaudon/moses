<?php
namespace App\Router;
require('../models/conf.php');

class Route
{
	private $path;
	private $callback;
	private $matches;
	private $params;

	public function __construct(string $path, $callback) 
	{
		$this->path = trim($path, '/');
		$this->callback = $callback;
		$this->matches = [];
		$this->params = [];
	}

	public function with(string $param, string $regex): Route
	{
		$this->params[$param] = str_replace('(', '(?:', $regex);
		return $this;
	}

	private function paramMatch(array $match): string
	{
		if (isset($this->params[$match[1]])) {
			return '('. $this->params[$match[1]] .')';
		}
		return '([^/]+)';
	}

	public function getUrl(array $params): string
	{
		$path = $this->path;
		foreach ($params as $k => $v) {
			$path = str_replace('{'.$k.'}', $v, $path);
		}
		return $path;
	}

	public function call()
	{
		if (is_string($this->callback)) {
			$params = explode('@', $this->callback);
			$controller = APPNAME.'\\Controller\\'. $params[0] .'Controller';
			$controller = new $controller();
			if (count($params) > 1) {
                return call_user_func_array(array($controller, $params[1]), $this->matches);
			} else {
    			return call_user_func_array(array($controller, 'show'), $this->matches);
			}
		} else {
			return call_user_func_array($this->callback, $this->matches);
		}
	}
	
	public function getPath(): string
	{
    	return $this->path;
	}
	
	public function setMatches(array $matches): void
	{
    	$this->matches = $matches;
	}
}