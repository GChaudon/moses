<?php
namespace App\Router;

class Router
{
    private static $url;
    private static $routes;
    public static $namedRoutes;
   	private static $defaultRoute;

    public static function init(): void
    {
    	self::$url = $_SERVER['REQUEST_URI'];
    	self::$routes = [];
    	self::$namedRoutes = [];
    }

    public static function setDefault($callback, ?string $name = null): void
    {
    	self::$defaultRoute = new Route('', $callback);
    }

    public static function get(string $path, $callback, ?string $name = null): Route
    {
        return self::add($path, $callback, $name, 'GET');
    }

    public static function post(string $path, $callback, ?string $name = null): Route
    {
        return self::add($path, $callback, $name, 'POST');
    }
    
    public static function view(string $path, string $view): Route
    {
        return self::add($path, function() use ($view) {
            require '../views/'. $view .'.php';
        }, null, 'GET');
    }

    private static function add(string $path, $callback, ?string $name, string $method): Route
    {
        $route = new Route($path, $callback);
        self::$routes[$method][] = $route;
        if (is_string($callback) && is_null($name)) {
            $name = $callback;
        }
        if ($name) {
            self::$namedRoutes[$name] = $route;
        }
        return $route;
    }

    public static function run()
    {
        if (!isset(self::$routes[$_SERVER['REQUEST_METHOD']])) {
            throw new RouterException('REQUEST_METHOD does not exist');
        }
        
        foreach (self::$routes[$_SERVER['REQUEST_METHOD']] as $route) {
            if (self::isMatchingRoute($route, self::$url)) {
                return $route->call();
            }
        }
        return self::$defaultRoute->call();
    }
    
    private static function isMatchingRoute(Route $route, string $url): bool
	{
		$url = trim($url, '/');
		$path = preg_replace('#{([a-zA-Z0-9]+)*}#', '([^/]+)', $route->getPath());
		$regex = '#^'. $path. '$#i';
		if (!preg_match($regex, $url, $matches)) {
			return false;
		}
		array_shift($matches);
		$route->setMatches($matches);
		return true;
	}

    public function url(string $name, ?array $params = [])
    {
        if (!isset(self::$namedRoutes[$name])) {
            return self::$defaultRoute->getUrl($params);
        }
        return self::$namedRoutes[$name]->getUrl($params);
    }

}