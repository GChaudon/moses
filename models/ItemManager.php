<?php
namespace App\Model;

class ItemManager extends Manager
{
	public function __construct() 
	{
		parent::__construct();
	}

	public function getItems(): array
	{
		$objects = [];

		$req = $this->db->query('SELECT * from objets');
		foreach($req->fetchAll() as $object) {
			$objects[] = new Item($object);
		}
		
		return $objects;
	}

	public function getItem(int $id): Item
	{
		$req = $this->db->prepare('SELECT * from objets where id = ?');
		$req->execute(array($id));
		$objRaw = $req->fetch();
		return new Item($objRaw);
	}
}