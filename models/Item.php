<?php

namespace App\Model;

class Item extends Model
{
	private $id;
	private $label;
	private $price;

	public function __construct(array $data) 
	{
		parent::__construct($data);
	}

	protected function callFunction(string $methodName, string $value = ""): void 
	{
		if(method_exists($this, $methodName)) {
			$this->$methodName($value);
		}
	}

	public function getId(): int
	{
		return $this->id;
	}

	public function getLabel(): string
	{
		return $this->label;
	}

	public function getPrice(): float
	{
		return $this->price;
	}

	private function setId(int $id): void
	{
		$this->id = $id;
	}

	private function setLabel(string $label): void
	{
		$this->label = $label;
	}

	private function setPrice(float $price): void
	{
		$this->price = $price;
	}
}